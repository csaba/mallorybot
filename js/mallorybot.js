

var jiraUrl = "https://www.ebi.ac.uk/panda/jira/rest";




function getDataFromJira(){

    fetch(
        jiraUrl+"/agile/1.0/board/137/sprint?state=active",
        {
            headers: {
            Authorization: 'Basic Y3NhYmE6ZWlNaWNoYWVsODIh',
            },
        },
    )
    .then(
        response => response.json(),
        (err) => alert('Failed to get metadata'),
    )
    .then(
        json => getTicketData(getActiveSprintId(json))
    );
}



function getTicketData(activeSprintId){

    fetch(
        jiraUrl+"/agile/1.0/board/137/sprint/"+ activeSprintId +"/issue ",
        {
            headers: {
            Authorization: 'Basic Y3NhYmE6ZWlNaWNoYWVsODIh',
            },
        },
    )
    .then(
        response => response.json(),
        (err) => alert('Failed to get ticket data'),
    )
    .then(
        json => createBotResponse(json)
    );

}


function createBotResponse(jsonData){

    let botResponse = "Sprint Review party time!\n";
    

    for(let i=0; i<jsonData["issues"].length; i++){

        if(jsonData["issues"][i]["fields"]["status"]["name"] == "Review"){

            let key = jsonData["issues"][i]["key"];
            let reviewer = "";
            botResponse += "https://www.ebi.ac.uk/panda/jira/browse/"+ key+ " ";


            if(jsonData["issues"][i]["fields"].hasOwnProperty("customfield_10831") && jsonData["issues"][i]["fields"]["customfield_10831"] != null){
                for (let j=0; j < jsonData["issues"][i]["fields"]["customfield_10831"].length; j++){
                    botResponse += getSlackId(jsonData["issues"][i]["fields"]["customfield_10831"][j]["name"]) + " ";
            }
            }

            botResponse += "\n";    

        }
    }
    
    botResponse += "#MalloryBot";

    insertBotResponse(botResponse);
}



function getSlackId(jiraUserName){


    return jiraUserName;
}


function insertBotResponse(botResponse){
    let txtArea = jQuery("#bot_result_txarea");
    txtArea.empty();
    txtArea.val(botResponse);
}


function getActiveSprintId(metadata){
    return metadata["values"][0]["id"];
}


function runMalloryBot(){
    console.log("Running bot");
    insertBotResponse("Fetching data from Jira API...");
    getDataFromJira();

}




